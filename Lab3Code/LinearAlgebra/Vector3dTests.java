//Oscar Palencia

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void test1() {
        Vector3d v = new Vector3d(2, 3, 4);
        assertEquals(2, v.getX());
        assertEquals(3, v.getY());
        assertEquals(4, v.getZ());
    }

    @Test
    public void test2() {
        Vector3d v = new Vector3d(2, 3, 6);
        assertEquals(7, v.magnitude());
    }

    @Test
    public void test3() {
        Vector3d v1 = new Vector3d(1, 1, 2);
        Vector3d v2 = new Vector3d(2, 3, 4);
        assertEquals(13, v1.dotProduct(v2));
    }

    @Test
    public void test4() {
        Vector3d v1 = new Vector3d(1, 1, 2);
        Vector3d v2 = new Vector3d(2, 3, 4);
        Vector3d v3 = v1.add(v2);
        assertEquals(3, v3.getX());
        assertEquals(4, v3.getY());
        assertEquals(6, v3.getZ());
    }
}