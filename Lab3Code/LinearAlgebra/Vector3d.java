//Oscar Palencia

package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d v) {
        double productX = this.x * v.getX();
        double productY = this.y * v.getY();
        double productZ = this.z * v.getZ();

        return productX + productY + productZ;
    }

    public Vector3d add(Vector3d v) {
        Vector3d newV = new Vector3d(this.x + v.getX(), this.y + v.getY(), this.z + v.getZ());
        return newV;
    }

    public static void main(String[] args) {
        Vector3d v1 = new Vector3d(2, 3, 4);
        System.out.println(v1.magnitude());

        Vector3d v2 = new Vector3d(1, 1, 2);
        System.out.println(v2.dotProduct(v1));

        Vector3d v3 = v1.add(v2);

        System.out.println(v3.getX()+" "+v3.getY()+" "+v3.getZ());
    }
}
